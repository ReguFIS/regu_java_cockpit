package keyword;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class KeyWordExecution {
	
	public static String methodName;
	public static String HTMLReportPath;
	public static String syspath;
	public static String driverpath;
	public static String ResultFolderPath;
	public static String TestCase_SheetName;
	public static int Not_Executed = 0;
	public static int TC_Passed = 0;
	public static int TC_Failed = 0;
	public static String TC_Return_value1;
	public static String TestScenario_Status;
	public static String IndTestcaseresult;
	static String HostName;
	static String OSName;
	static String Absolute_File_Path;
	static String Project_Name;
	static String TestCase_ExcelSheet;
	static String FolderName;
	static String Resultpath;
	static String restemppath;
	static String Heading;
	static String TestCase_Execution;
	static String TestCase_InputSheet;
	static String TestCase_InputType;
	static String TestCase_TestDataSheet;
	static String Testdatavariable;
	static String Tcname;
	static String Parametername;
	static String TC_Name;
	static String Testdatavariable1;
	static String Details;
	static String Status;
	
	public static void main(String[] args) throws IOException {
    	
    	KeyWordExecution exeKey = new KeyWordExecution();
        ReadExcel excelSheet1 	= new ReadExcel();
        ReadExcel excelSheet2 	= new ReadExcel();
        HTMLReport Report 		= new HTMLReport();
		GeneralFunctions Gnfn	= new GeneralFunctions();
		
		HostName =  InetAddress.getLocalHost().getHostName();
		System.out.println("Host Name:- "+ HostName);
				
		OSName = System.getProperty("os.name");
		System.out.println("OS Name:- " + OSName);
		
		Date Start_Time = new Date();
	    System.out.println(Start_Time);
        
// To get the absolute path of the working folder		
		Absolute_File_Path = new File("src\\Keyword\\KeyWordExecution.java").getAbsolutePath().toString();
		String[] Path_Split = Absolute_File_Path.split("\\\\");
		syspath = Path_Split[0] +"\\" + Path_Split[1] +"\\" + Path_Split[2]+"\\" + Path_Split[3];
		Project_Name = Path_Split[3];
		
		driverpath = Path_Split[0] +"\\" + Path_Split[1];
		TestCase_ExcelSheet = syspath + "\\Input_Files\\Test_Case.xlsx";
		
        System.out.println("Absolute Path of the Working folder:- " + syspath);
        System.out.println("Project Name is:- "+ Project_Name);
        
// To create the results folder
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		FolderName = dateFormat.format(Start_Time);
		Resultpath = Path_Split[0] +"\\" + Path_Split[1] + "\\Output_Files";
		Gnfn.createFolder(Resultpath, FolderName);
		ResultFolderPath = Resultpath + "\\" + FolderName;

// Creating template for generating TestExecutionreport.html
		restemppath = ResultFolderPath + "\\"+ "TestExecutionReport.html";
		Heading  = "Execution Summary - " + Project_Name + " - Execution Completed";
		Report.TestExecutionReportTemplate(restemppath, Heading);
		
//  Reading Test_Case.xlsx sheet      
		excelSheet1.openSheet(TestCase_ExcelSheet);
        for(int i=1; i < excelSheet1.getRowCount(); i++) {
        	
        TestCase_SheetName = excelSheet1.getValueFromCell(i, 0).trim();
        TestCase_Execution = excelSheet1.getValueFromCell(i, 1).trim();
        TestCase_InputSheet= excelSheet1.getValueFromCell(i, 2).trim();
        TestCase_InputType = excelSheet1.getValueFromCell(i, 3).trim();
        
    	IndTestcaseresult = ResultFolderPath + "\\" + TestCase_SheetName ;
		HTMLReportPath = IndTestcaseresult + "\\" + TestCase_SheetName + ".html";
			
        ReadExcel excelTestdatasheet = new ReadExcel();
		TestCase_TestDataSheet = syspath + "\\Input_Files\\Test_Data.xlsx";
		
        if (TestCase_Execution.equalsIgnoreCase("YES")) {
        excelSheet2.openTCSheet(TestCase_ExcelSheet,TestCase_SheetName);                           
        
        Gnfn.createFolder(ResultFolderPath, TestCase_SheetName);
        Report.createResultsfile1(HTMLReportPath, TestCase_SheetName);
        
        ArrayList <String> TC_Return_value_Array = new ArrayList<String>();
        
        switch (TestCase_InputType) {
        
        case "Single Data Set":
        	if (TestCase_Execution.equalsIgnoreCase("YES")) {
            excelSheet2.openTCSheet(TestCase_ExcelSheet,TestCase_SheetName);
            	
            
            
            	for(int row=1; row < excelSheet2.getRowCount(); row++)           {              
                List<Object> myParamList = new ArrayList<Object>();
                methodName 	= excelSheet2.getValueFromCell(row,1).trim();
              
                Testdatavariable = excelSheet2.getValueFromCell(row,4).trim();
                	
                	for(int col=0; col < excelSheet2.getColumnCount(row); col++) {
                		if (!excelSheet2.getValueFromCell(row, col).isEmpty() & !excelSheet2.getValueFromCell(row, col).equals("null")) {
                			myParamList.add(excelSheet2.getValueFromCell(row, col));
                            }	
                	}
                
                	excelTestdatasheet.openTCSheet(TestCase_TestDataSheet,TestCase_InputSheet);
                	
		                for(int rows=1;rows<excelTestdatasheet.getRowCount();rows++){
		                Tcname = excelTestdatasheet.getValueFromCell(rows, 0);
		                	
		                if (!Testdatavariable.isEmpty()) {
		                	if (Tcname.equals(TestCase_SheetName))	{
		                	for(int cols=1;cols<excelTestdatasheet.getColumnCount(rows);cols++){
		                    Parametername = excelTestdatasheet.getValueFromCell(rows, cols);       
		                    	
		                    	if (Testdatavariable.equals(Parametername)&&myParamList.size()==3) {
		                    	myParamList.remove(2);
		                        myParamList.remove(1);  
		                        myParamList.add(excelTestdatasheet.getValueFromCell(rows, cols + 1));
		                        break; 
		                        }
		                    	else if (Testdatavariable.equals(Parametername)&&myParamList.size()==5) {
		                        myParamList.remove(4);
		                        myParamList.remove(1);
		                        myParamList.add(excelTestdatasheet.getValueFromCell(rows, cols + 1));
		                        break;
		                        }
		                     }  
		                }	 
		                }
		                else  {
		                	myParamList.remove(1);
		                	break;	}             
		                }
		                
				Object[] paramListObject = new String[myParamList.size()];
				paramListObject = myParamList.toArray(paramListObject);
				Object TC_Return_value = exeKey.runReflectionMethod("keyword.Keywords", methodName, paramListObject);
				
				TC_Return_value1 = TC_Return_value.toString();
				TC_Return_value_Array.add(TC_Return_value1);
				}  
            	
            	if(TC_Return_value_Array.contains("Failed")){
            		TC_Failed = TC_Failed + 1;
            		TestScenario_Status = "Fail";
            		break;
        		}
            	else {
					TC_Passed = TC_Passed + 1;
					TestScenario_Status = "Pass";
            	}
           	
 			break;
         }                                              
                                                                
		case "Multiple Data Set":   
			if (TestCase_Execution.equalsIgnoreCase("YES")) {
			excelSheet2.openTCSheet(TestCase_ExcelSheet,TestCase_SheetName);
			excelTestdatasheet.openTCSheet(TestCase_TestDataSheet,TestCase_InputSheet);
				
				for(int rwcnt=1;rwcnt<excelTestdatasheet.getRowCount();rwcnt++){
                    for(int rowsm=1;rowsm<excelSheet2.getRowCount();rowsm++){
                    List<Object> myParamList1 = new ArrayList<Object>();
                    methodName = excelSheet2.getValueFromCell(rowsm, 1);
                    TC_Name = excelSheet2.getValueFromCell(rowsm, 0);
                    Testdatavariable1 = excelSheet2.getValueFromCell(rowsm,4);
                    
                    for(int colm=0; colm < excelSheet2.getColumnCount(rowsm); colm++) {
                    	if (!excelSheet2.getValueFromCell(rowsm, colm).isEmpty() & !excelSheet2.getValueFromCell(rowsm, colm).equals("null")) {
                        myParamList1.add(excelSheet2.getValueFromCell(rowsm, colm));
                        }
                        String TC_Name_1 = TC_Name +"_" +rwcnt;
                        myParamList1.remove(0);
                        myParamList1.add(0, TC_Name_1);
                    }
                    
						if(!Testdatavariable1.isEmpty()){
			                for(int colcnt=0;colcnt<excelTestdatasheet.getColumnCount(0);colcnt++){
			                String val=excelTestdatasheet.getValueFromCell(0,colcnt);
			                	if(Testdatavariable1.equals(val)&&myParamList1.size()==3){
			                	myParamList1.remove(2);
			                	myParamList1.remove(1);
			                    myParamList1.add(excelTestdatasheet.getValueFromCell(rwcnt,colcnt));
			                    break;
			                	}
			                	else if(Testdatavariable1.equals(val)&&myParamList1.size()==5){
			                    myParamList1.remove(4);
			                    myParamList1.remove(1);
			                    myParamList1.add(excelTestdatasheet.getValueFromCell(rwcnt,colcnt));
			                    break;
			                    }
			                }                              
						}              
						else  {
		                	myParamList1.remove(1);
		                	}    
		                 
						
	                Object[] paramListObject1 = new String[myParamList1.size()];
	                paramListObject1 = myParamList1.toArray(paramListObject1);
	                Object TC_Return_value = exeKey.runReflectionMethod("keyword.Keywords", methodName, paramListObject1); 
	                
					TC_Return_value1 = TC_Return_value.toString();
					TC_Return_value_Array.add(TC_Return_value1);
					
                    }
                }
				
				if(TC_Return_value_Array.contains("Failed")){
            		TC_Failed = TC_Failed + 1;
            		TestScenario_Status = "Fail";
            		break;
        		}
            	else {
					TC_Passed = TC_Passed + 1;
					TestScenario_Status = "Pass";
            	}
				break;
				
			}              
        }
      
	        Details = "";
	    	Report.TestExecutionReportUpdate(restemppath, HTMLReportPath, TestCase_SheetName, TestScenario_Status, Details);
        }                                                                                              
                
        else if(TestCase_Execution.equalsIgnoreCase("NO")) {
	        Not_Executed = Not_Executed + 1;
	    	System.out.println("TC not Executed:-"+TestCase_SheetName);
	    	TestScenario_Status = "Not Executed";
	        Details = "This test case is set to NO in TC_Sheet";
	        HTMLReportPath = "";
	        Report.TestExecutionReportUpdate(restemppath, HTMLReportPath, TestCase_SheetName, TestScenario_Status, Details);
        }
     }
		Date End_Time = new Date();
	    System.out.println(End_Time);
	    
	    Report.TestExecutionReportStatusUpdate(restemppath, Project_Name, Start_Time, End_Time, HostName, OSName, TC_Passed, TC_Failed, Not_Executed);   
	   
	}
                   
	
	
    public String runReflectionMethod (String strClassName, String strMethodName, Object... inputArgs) {
    	Class<?> params[] = new Class[inputArgs.length];
    	
    	for (int i = 0; i < inputArgs.length; i++) {
    		if (inputArgs[i] instanceof String) {
            params[i] = String.class;
            }
        }
        try {
        Class<?> cls = Class.forName(strClassName);
        Object _instance = cls.newInstance();
        Method myMethod = cls.getDeclaredMethod(strMethodName, params);
        Object Invoked_Method_Status = myMethod.invoke(_instance, inputArgs);
        String return_Status =  Invoked_Method_Status.toString();
        return return_Status;
        
        } catch (ClassNotFoundException e) {
        	System.err.format(strClassName + ":- Class not found%n");
        } catch (IllegalArgumentException e) {
        	System.err.format("Method invoked with wrong number of arguments%n");
        } catch (NoSuchMethodException e) {
        	System.err.format("In Class " + strClassName + "::" + strMethodName  + ":- method does not exists%n");
        } catch (InvocationTargetException e) {
        	System.err.format("Exception thrown by an invoked method%n");
        } catch (IllegalAccessException e) {
        	System.err.format("Can not access a member of class with modifiers private%n");
        e.printStackTrace();
        } catch (InstantiationException e) {
        	System.err.format("Object cannot be instantiated for the specified class using the newInstance method%n");
        }
        String return_value = "Exception Thrown";
		return return_value;
    	}
                                
}
