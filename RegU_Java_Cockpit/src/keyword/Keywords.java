package keyword;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Keywords {
	

	public static WebDriver driver;
	static WebDriverWait wait;
	static String testData;
	static String expected;
	static String actual;
	static String status;
	static String Method_Status;
	
	
	HTMLReport Report 	= new HTMLReport();
	
	String action_perform = KeyWordExecution.methodName;
	String HTMLpath 	  = KeyWordExecution.HTMLReportPath;
	String systempath 	  = KeyWordExecution.syspath;
	String driver_path    = KeyWordExecution.driverpath;
	String TCFolderpath	  = KeyWordExecution.IndTestcaseresult;
	String TCName		  = KeyWordExecution.TestCase_SheetName;
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	open_Browser
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public String open_Browser (String TestCase_ID, String browserName) throws IOException {
		try {
			if (browserName.equalsIgnoreCase("Firefox")) {
				driver = new FirefoxDriver();
			} else if (browserName.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver","D:/Jars/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browserName.equalsIgnoreCase("IE")) {
				System.setProperty("webdriver.ie.driver", driver_path +"\\"+ "IEDriverServer_32bit.exe");
				driver = new InternetExplorerDriver();
			}
			testData = browserName;
			expected = "";
			actual = "";
			status = "Pass";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.out.println(e.getMessage());
			status = e.getMessage();
			testData = browserName;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}	

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	enter_URL
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public String enter_URL (String TestCase_ID, String URL) throws IOException {
		try{
			driver.navigate().to(URL);
			testData = URL;
			expected = "";
			actual = "";
			status = "Pass";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
		}
		catch (Exception e){
			System.out.println(e.getMessage());
			testData = URL;
			status = "Fail";
			expected = "";
			actual = "Driver Not Found";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	locatorValue
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 
	
	public By locatorValue (String locatorTpye, String value) {
		By by;
		switch (locatorTpye) {
		case "id":
			by = By.id(value);
			break;
		case "name":
			by = By.name(value);
			break;
		case "xpath":
			by = By.xpath(value);
			break;
		case "css":
			by = By.cssSelector(value);
			break;
		case "linkText":
			by = By.linkText(value);
			break;
		case "partialLinkText":
			by = By.partialLinkText(value);
			break;
		default:
			by = null;
			break;
		}
		return by;
	}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	enter_Text
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String enter_Text (String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			element.sendKeys(text);
			testData = text;
			expected = "";
			actual = "";
			status = "Pass";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			} 	
		catch (Exception e) {
			System.err.format("No Element Found to enter text" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
			}
	}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	Clear_Text
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String Clear_Text (String TestCase_ID, String locatorType, String value) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			element.clear();
			testData = value;
			expected = "";
			actual = "";
			status = "Pass";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			} 	
		catch (Exception e) {
			System.err.format("No Element Found to enter text" + e);
			testData = value;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	
	}
	 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	click_On_Link
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String click_On_Link (String TestCase_ID, String locatorType, String value) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			element.click();
			expected = "";
			actual = "";
			status = "Pass";
			testData = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Click link" + e);
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			testData = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
			
		}
		
	}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	click_On_Button
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String click_On_Button (String TestCase_ID, String locatorType, String value) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			element.click();
			expected = "";
			actual = "";
			status = "Pass";
			testData = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to click button" + e);
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			testData = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
			
		}
		
	}
	

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	Select_Checkbox
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String Select_Checkbox (String TestCase_ID, String locatorType, String value) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			element.click();
			expected = "";
			actual = "";
			status = "Pass";
			testData = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Select Checkbox" + e);
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			testData = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
			}
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	Select_Radiobutton
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String Select_Radiobutton (String TestCase_ID, String locatorType, String value) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			element.click();
			expected = "";
			actual = "";
			status = "Pass";
			testData = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Select Radiobutton" + e);
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			testData = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
			
		}
	}

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	Select_Dropdown
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String Select_Dropdown (String TestCase_ID, String locatorType, String value, String DDValue) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement dropDownn = driver.findElement(locator);
			Select se = new Select(dropDownn);
			se.selectByValue(DDValue);
			expected = "";
			actual = "";
			status = "Pass";
			testData = "Value = " + DDValue;
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			}
		catch (Exception e) {
			System.err.format("No Element Found to Select value from dropdown" + e);
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			testData = "Value = " + DDValue;
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	
	}

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	Select_Dropdown_text
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String Select_Dropdown_text (String TestCase_ID, String locatorType, String value, String DDValue) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement dropDownn = driver.findElement(locator);
			Select se = new Select(dropDownn);
			se.selectByVisibleText(DDValue);
			expected = "";
			actual = "";
			status = "Pass";
			testData = DDValue;
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			}
		catch (Exception e) {
			System.err.format("No Element Found to Select value from dropdown" + e);
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			testData = DDValue;
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	close_Browser
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String close_Browser (String TestCase_ID) throws IOException { 
		try{
			driver.quit();
			expected = "";
			actual = "";
			status = "Pass";
			testData = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			}
		catch (Exception e)	{
			System.err.format("No Driver Found  " + e);
			testData = "";
			expected = "";
			actual = "";
			status = "Fail";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
			}
	}
	

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	wait_Time
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String wait_Time (String TestCase_ID, String timeValue) throws IOException {
		try {
			int time  = Integer.parseInt(timeValue);
			Thread.sleep(time);
			testData = timeValue;
			expected = "";
			actual = "";
			status = "Pass";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			testData = timeValue;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}	

	}
	

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	Screenshot
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String Screenshot(String TestCase_ID) throws IOException {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(TCFolderpath+"\\"+TestCase_ID+"_"+TCName+".png"));
			testData = "";
			expected = "";
			actual = "";
			status = "Pass";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (IOException e) {
			testData = "";
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			 Method_Status = "Failed";
			return Method_Status;
		}	
	}	


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	Element_Validation
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String Element_Validation (String TestCase_ID, String locatorType, String value, String ValidationType) throws IOException {
		try{
			By locator;
			locator = locatorValue(locatorType, value);
		
			switch (ValidationType) {
			
			case "isEnabled":
				boolean ObjectEnabled = driver.findElement(locator).isEnabled();
					if(ObjectEnabled)	{
						System.out.println("*****Element is Enabled*****");
						testData = ValidationType;
						expected = "";
						actual = "Element is Enabled";
						status = "Pass";
						Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
						Method_Status = "Passed";
						return Method_Status;	}
					else	{
						System.out.println("*****Element is not Enabled*****");	
						testData = ValidationType;
						expected = "";
						actual = "Element is not Enabled";
						status = "Fail";
						Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
						Method_Status = "Failed";
						return Method_Status;	}
					
			case "isSelected":
				boolean ObjectSelected = driver.findElement(locator).isSelected();
					if(ObjectSelected)	{
						System.out.println("*****Element is Selected*****");
						testData = ValidationType;
						expected = "";
						actual = "Element is Selected";
						status = "Pass";
						Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
						Method_Status = "Passed";
						return Method_Status;	}
					else	{
						System.out.println("*****Element is not Selected*****");	
						testData = ValidationType;
						expected = "";
						actual = "Element is not Selected";
						status = "Fail";
						Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
						Method_Status = "Failed";
						return Method_Status;	}
				
			case "isDisplayed":
				boolean ObjectDisplayed = driver.findElement(locator).isDisplayed();
					if(ObjectDisplayed)	{
						System.out.println("*****Element is Displayed*****");
						expected = "";
						actual = "Element is Displayed";
						status = "Pass";
						testData = ValidationType;
						Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
						Method_Status = "Passed";
						return Method_Status;}
					else	{
						System.out.println("*****Element is not Displayed*****");
						expected = "";
						actual = "Element is not Displayed";
						status = "Fail";
						testData = ValidationType;
						Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
						Method_Status = "Failed";
						return Method_Status;}
					}
				Method_Status = "Passed";
				return Method_Status;
			}
			catch(Exception e){
				expected = "";
				actual = "";
				testData = "";
				String Error_Msg[] = e.getMessage().split("WARNING");
				status = Error_Msg[0].replace("(", "");
				Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
				Method_Status = "Failed";
				return Method_Status;
			}
	}
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	Text_Validation
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String Text_Validation (String TestCase_ID, String locatorType, String value, String ExpectedText) throws IOException {
		try{
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			actual = element.getText().trim();
			expected = ExpectedText;
					
			System.out.println("Expected Value is -"+ ExpectedText);
			System.out.println("Actual Value is -"+ actual);
			
					if (ExpectedText.trim().equals(actual)){
						System.out.println("*****Text is Equal*****");
						status = "Pass";
						testData = "";
						Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
						Method_Status = "Passed";
						return Method_Status;
					}
					else {
						System.out.println("*****Text is not Equal*****");
						status = "Fail";
						testData = "";
						Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
						Method_Status = "Failed";
						return Method_Status;
						}	
		}
		catch (Exception e){
			actual = "";
			testData = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, ExpectedText, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	checkAlert_Yes
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public String checkAlert_Yes (String TestCase_ID) throws IOException {
	    try {
	        WebDriverWait wait = new WebDriverWait(driver, 2);
	        wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
			status = "Pass";
			testData = "";
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
	    	} 
	    catch (Exception e) {
	    	testData = "";
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
	    }
	}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////	File_Upload
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public String File_Upload (String TestCase_ID, String locatorType, String value, String File_Path) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			element.sendKeys(File_Path);
			status = "Pass";
			testData = File_Path;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			}
		catch (NoSuchElementException e) {
			System.err.format("No Element Found to Upload File" + e);
	    	testData = File_Path;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
			}
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Switch to Iframe
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	public String Switch_Iframe (String TestCase_ID, String Iframe) throws IOException {
		try {
		//	By locator;
		//	locator = locatorValue(locatorType, value);
			driver.switchTo().frame(Iframe);
			testData = Iframe;
			expected = "";
			actual = "";
			status = "Pass";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
		} 	
		catch (Exception e) {
			System.err.format("No Element Found to Switch to Iframe" + e);
			testData = Iframe;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////Switch to Main frame
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	public String Switch_Mainframe (String TestCase_ID) throws IOException {
		try {
			driver.switchTo().defaultContent();
			testData = "";
			expected = "";
			actual = "";
			status = "Pass";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
		} 	
		catch (Exception e) {
			System.err.format("No Element Found to Switch to Main Frame" + e);
			testData = "";
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////Scroll Up
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	public String Scroll_Up (String TestCase_ID) throws IOException {
		try {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,-250)", "");			
			testData = "";
			expected = "";
			actual = "";
			status = "Pass";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Passed";
			return Method_Status;
		} 	
		catch (Exception e) {
			System.err.format("No Element Found to Switch to Main Frame" + e);
			testData = "";
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}

//////////////////////////////////////////////////////////////////////////////////////////////////
//					Application Specific Functions
//////////////////////////////////////////////////////////////////////////////////////////////////

// Edit LOB
	
    public String Edit_LOB (String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			String customer=element.findElement(By.xpath("//tr/td/span[contains(text(),'"+text+"')]")).getAttribute("id");
			String[] trid = customer.split("(?<=\\D)(?=\\d)");
			String noOnly = String.valueOf(trid[1]);             
			WebElement newRow = element.findElement(By.xpath("//tr[contains(@id, '" +noOnly+ "')]/td[3]/input[1]"));
			newRow.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Edit the LOB" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}
    
	
// Delete LOB
    
	public String Delete_LOB (String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			String customer=element.findElement(By.xpath("//tr/td/span[contains(text(),'"+text+"')]")).getAttribute("id");
			String[] trid = customer.split("(?<=\\D)(?=\\d)");
			String noOnly = String.valueOf(trid[1]);             
			WebElement newRow1 = element.findElement(By.xpath("//tr[contains(@id, '" +noOnly+ "')]/td[3]/input[2]"));
			newRow1.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Delete the LOB" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}
	
	
//Edit Product	
	
	public String Edit_Product (String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			String customer=element.findElement(By.xpath("//tr/td/span[contains(text(),'"+text+"')]")).getAttribute("id");
			String[] trid = customer.split("(?<=\\D)(?=\\d)");
			String noOnly = String.valueOf(trid[1]);             
			WebElement newRow2 = element.findElement(By.xpath("//tr[contains(@id, '" +noOnly+ "')]/td[4]/input[1]"));
			newRow2.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Edit the Product" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}
	
	
//Delete Product	
	public String Delete_Product (String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			String customer=element.findElement(By.xpath("//tr/td/span[contains(text(),'"+text+"')]")).getAttribute("id");
			String[] trid = customer.split("(?<=\\D)(?=\\d)");
			String noOnly = String.valueOf(trid[1]);             
			WebElement newRow3 = element.findElement(By.xpath("//tr[contains(@id, '" +noOnly+ "')]/td[4]/input[2]"));
			newRow3.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Delete the Product" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}

	
//Edit Rule	
	public String Edit_Rule(String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			String customer=element.findElement(By.xpath("//tr/td/span[contains(text(),'"+text+"')]")).getAttribute("id");
			String[] trid = customer.split("(?<=\\D)(?=\\d)");
			String noOnly = String.valueOf(trid[1]);             
			WebElement newRow4 = element.findElement(By.xpath("//tr[contains(@id, '" +noOnly+ "')]/td[4]/input[1]"));
			newRow4.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Edit the Rule" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}

//Delete Rule	
	public String Delete_Rule(String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			String customer=element.findElement(By.xpath("//tr/td/span[contains(text(),'"+text+"')]")).getAttribute("id");
			String[] trid = customer.split("(?<=\\D)(?=\\d)");
			String noOnly = String.valueOf(trid[1]);             
			WebElement newRow5 = element.findElement(By.xpath("//tr[contains(@id, '" +noOnly+ "')]/td[4]/input[2]"));
			newRow5.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Delete the Rule" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}	
	
//	Select the Product Rules
	public String ProductRules(String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			element.findElement(By.xpath("//tr/td/span[contains(text(),'"+ text +"')]/ancestor::tr/td[2]/input[@type='checkbox']")).click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Select the Rule" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}	


//Edit Assertion	
	public String Edit_Assertion(String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			String customer=element.findElement(By.xpath("//tr/td/span[contains(text(),'"+text+"')]")).getAttribute("id");
			String[] trid = customer.split("(?<=\\D)(?=\\d)");
			String noOnly = String.valueOf(trid[1]);             
			WebElement newRow6 = element.findElement(By.xpath("//tr[contains(@id, '" +noOnly+ "')]/td[3]/input[1]"));
			newRow6.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Delet the Rule" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}
	
//Delete Assertion	
	
	public String Delete_Assertion(String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			String customer=element.findElement(By.xpath("//tr/td/span[contains(text(),'"+text+"')]")).getAttribute("id");
			String[] trid = customer.split("(?<=\\D)(?=\\d)");
			String noOnly = String.valueOf(trid[1]);             
			WebElement newRow7 = element.findElement(By.xpath("//tr[contains(@id, '" +noOnly+ "')]/td[3]/input[2]"));
			newRow7.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Delet the Rule" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}	
	
//Insert above Assertion
	
	public String Insert_Above(String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);
			String customer=element.findElement(By.xpath("//tr/td/span[contains(text(),'"+text+"')]")).getAttribute("id");
			String[] trid = customer.split("(?<=\\D)(?=\\d)");
			String noOnly = String.valueOf(trid[1]);             
			WebElement newRow8 = element.findElement(By.xpath("//tr[contains(@id, '" +noOnly+ "')]/td[3]/input[3]"));
			newRow8.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Delet the Rule" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}
	
//Search user
	public String Search_User(String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);            
		    WebElement htmltable1= element.findElement(By.xpath("//tr/td/a[contains(text(),'"+text+"')]"));
		    htmltable1.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Delet the Rule" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}	
	
//Search Institution
	
	public String Search_Institution(String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);            
		    WebElement htmltable2= element.findElement(By.xpath("//tr/td/a[contains(text(),'"+text+"')]"));
		    htmltable2.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Delet the Rule" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
		}
	}	
	
//Search Enforcement
	
	public String Edit_Enforce(String TestCase_ID, String locatorType, String value, String text) throws IOException {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebElement element = driver.findElement(locator);            
		    WebElement htmltable3= element.findElement(By.xpath("//tr/td/a/span[contains(text(),'"+text+"')]"));
		    htmltable3.click();
			status = "Pass";
			testData = text;
			expected = "";
			actual = "";
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);	
			Method_Status = "Passed";
			return Method_Status;
			} 
		catch (Exception e) {
			System.err.format("No Element Found to Delet the Rule" + e);
			testData = text;
			expected = "";
			actual = "";
			String Error_Msg[] = e.getMessage().split("WARNING");
			status = Error_Msg[0].replace("(", "");
			Report.create_TCResults1 (HTMLpath, TestCase_ID, action_perform, testData, expected, actual, status);
			Method_Status = "Failed";
			return Method_Status;
			}
	}				
					
				
}

